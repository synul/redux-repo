import { EntityId } from '.';
export declare function isEntityId(test: unknown): test is EntityId;
export declare function isEntityIdArray(test: unknown): test is EntityId[];
