import { Dispatch, Update } from '@reduxjs/toolkit';
import { EntityState, RepoFetchOpts, RepoOpts } from '.';
export declare function createRepo<Entity extends any>({ name, selectId, extraReducers, sortComparer, }: RepoOpts<Entity>): {
    adapter: import("@reduxjs/toolkit").EntityAdapter<Entity>;
    fetch: <Result extends string | number | (string | number)[] | Entity | Entity[]>({ fetchFn, fetchFnArgs, operation, processor, }: RepoFetchOpts<Result>) => (dispatch: Dispatch<import("redux").AnyAction>, getState: () => EntityState<Entity>) => Promise<any>;
    slice: import("@reduxjs/toolkit").Slice<import("@reduxjs/toolkit").EntityState<Entity> & EntityState<Entity>, {
        addMany: {
            <S extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S, entities: Record<string | number, Entity> | Entity[]): S;
            <S_1 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_1 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_1, entities: {
                payload: Record<string | number, Entity> | Entity[];
                type: string;
            }): S_1;
        };
        addOne: {
            <S_2 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_2 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_2, entity: Entity): S_2;
            <S_3 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_3 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_3, action: {
                payload: Entity;
                type: string;
            }): S_3;
        };
        removeAll: <S_4 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_4 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_4) => S_4;
        removeMany: {
            <S_5 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_5 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_5, keys: (string | number)[]): S_5;
            <S_6 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_6 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_6, keys: {
                payload: (string | number)[];
                type: string;
            }): S_6;
        };
        removeOne: {
            <S_7 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_7 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_7, key: string | number): S_7;
            <S_8 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_8 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_8, key: {
                payload: string | number;
                type: string;
            }): S_8;
        };
        setAll: {
            <S_9 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_9 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_9, entities: Record<string | number, Entity> | Entity[]): S_9;
            <S_10 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_10 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_10, entities: {
                payload: Record<string | number, Entity> | Entity[];
                type: string;
            }): S_10;
        };
        updateMany: {
            <S_11 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_11 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_11, updates: Update<Entity>[]): S_11;
            <S_12 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_12 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_12, updates: {
                payload: Update<Entity>[];
                type: string;
            }): S_12;
        };
        updateOne: {
            <S_13 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_13 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_13, update: Update<Entity>): S_13;
            <S_14 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_14 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_14, update: {
                payload: Update<Entity>;
                type: string;
            }): S_14;
        };
        upsertMany: {
            <S_15 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_15 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_15, entities: Record<string | number, Entity> | Entity[]): S_15;
            <S_16 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_16 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_16, entities: {
                payload: Record<string | number, Entity> | Entity[];
                type: string;
            }): S_16;
        };
        upsertOne: {
            <S_17 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_17 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_17, entity: Entity): S_17;
            <S_18 extends import("@reduxjs/toolkit").EntityState<Entity>>(state: boolean extends (S_18 extends never ? true : false) ? import("@reduxjs/toolkit").EntityState<Entity> : S_18, entity: {
                payload: Entity;
                type: string;
            }): S_18;
        };
        setIdle: (state: {
            ids: (string | number)[];
            entities: {
                [x: string]: import("@reduxjs/toolkit").Draft<Entity> | import("@reduxjs/toolkit").Draft<undefined & Entity>;
                [x: number]: import("@reduxjs/toolkit").Draft<Entity> | import("@reduxjs/toolkit").Draft<undefined & Entity>;
            };
            loading: "idle" | "pending";
        }) => void;
        setPending: (state: {
            ids: (string | number)[];
            entities: {
                [x: string]: import("@reduxjs/toolkit").Draft<Entity> | import("@reduxjs/toolkit").Draft<undefined & Entity>;
                [x: number]: import("@reduxjs/toolkit").Draft<Entity> | import("@reduxjs/toolkit").Draft<undefined & Entity>;
            };
            loading: "idle" | "pending";
        }) => void;
    }, string>;
};
