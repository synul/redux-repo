export function isEntityId(test) {
    return (typeof test === 'number'
        || typeof test === 'string');
}
export function isEntityIdArray(test) {
    return (Array.isArray(test)
        && test.every(item => isEntityId(item)));
}
