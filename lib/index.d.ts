import { Comparer, Draft, IdSelector, PayloadAction } from '@reduxjs/toolkit';
export { Draft } from '@reduxjs/toolkit';
export * from './createRepo';
export declare type EntityId = string | number;
export interface EntityState<T> {
    entities: Record<EntityId, T>;
    ids: EntityId[];
    loading: 'idle' | 'pending';
}
export interface RepoOpts<Entity> {
    name: string;
    selectId?: IdSelector<Entity>;
    extraReducers?: RepoExtraReducersMap<EntityState<Entity>>;
    sortComparer?: Comparer<Entity>;
}
export declare type RepoReducerFn<S, P> = (state: Draft<S>, action: PayloadAction<P>) => void;
export declare type RepoExtraReducersMap<State> = {
    [K: string]: RepoReducerFn<State, unknown>;
};
export declare type RepoFetchOpts<FetchResult> = {
    fetchFn: (...args: any[]) => Promise<any>;
    operation: ReduceOperation;
    processor?: (result: FetchResult, state: unknown) => FetchResult;
    fetchFnArgs?: any[];
};
export declare enum ReduceOperation {
    AddMany = "AddMany",
    AddOne = "AddOne",
    RemoveAll = "RemoveAll",
    RemoveMany = "RemoveMany",
    RemoveOne = "RemoveOne",
    SetAll = "SetAll",
    UpdateMany = "UpdateMany",
    UpdateOne = "UpdateOne",
    UpsertMany = "UpsertMany",
    UpsertOne = "UpsertOne"
}
declare type Processor = <Result>(fetchResult: unknown) => Result;
declare type ErrorHandler = (err: Error) => void;
export declare type ReduxRepoConfig = {
    /**
     * Will receive the raw response of a repo's fetch call. Return value will be passed to reducers.
     */
    defaultProcessor?: Processor;
    /**
     * Will be passed any errors that occur during fetching. Use this to centrally display errors via a Toast or similar.
     */
    errorHandler?: ErrorHandler;
};
declare class ReduxRepo {
    defaultProcessor: Processor;
    errorHandler: ErrorHandler;
    init({ defaultProcessor, errorHandler, }: ReduxRepoConfig): void;
}
declare const reduxRepo: ReduxRepo;
export default reduxRepo;
