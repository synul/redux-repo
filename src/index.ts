import {
  Comparer,
  Draft,
  IdSelector,
  PayloadAction,
} from '@reduxjs/toolkit';

export { Draft } from '@reduxjs/toolkit';

export * from './createRepo';

export type EntityId = string | number;

export interface EntityState<T> {
  entities: Record<EntityId, T>;
  ids: EntityId[];
  loading: 'idle' | 'pending';
}

export interface RepoOpts<Entity> {
  name: string,
  selectId?: IdSelector<Entity>,
  extraReducers?: RepoExtraReducersMap<EntityState<Entity>>,
  sortComparer?: Comparer<Entity>,
}

export type RepoReducerFn<S, P> = (state: Draft<S>, action: PayloadAction<P>) => void;

export type RepoExtraReducersMap<State> = {
  [K: string]: RepoReducerFn<State, unknown>;
}

export type RepoFetchOpts<FetchResult> = {
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  fetchFn: (...args: any[]) => Promise<any>;
  operation: ReduceOperation;
  processor?: (result: FetchResult, state: unknown) => FetchResult;
  /* eslint-disable-next-line @typescript-eslint/no-explicit-any */
  fetchFnArgs?: any[];
}

export enum ReduceOperation {
  AddMany = 'AddMany',
  AddOne = 'AddOne',
  RemoveAll = 'RemoveAll',
  RemoveMany = 'RemoveMany',
  RemoveOne = 'RemoveOne',
  SetAll = 'SetAll',
  UpdateMany = 'UpdateMany',
  UpdateOne = 'UpdateOne',
  UpsertMany = 'UpsertMany',
  UpsertOne = 'UpsertOne',
}


type Processor = <Result>(fetchResult: unknown) => Result;
type ErrorHandler = (err: Error) => void;

export type ReduxRepoConfig = {
  /**
   * Will receive the raw response of a repo's fetch call. Return value will be passed to reducers.
   */
  defaultProcessor?: Processor;
  /**
   * Will be passed any errors that occur during fetching. Use this to centrally display errors via a Toast or similar.
   */
  errorHandler?: ErrorHandler;
}

class ReduxRepo {
  public defaultProcessor: Processor = <Result>(res: unknown) => res as Result;
  public errorHandler: ErrorHandler = (err) => { console.error('[redux-repo] Error: ', err) };

  init({
    defaultProcessor,
    errorHandler,
  }: ReduxRepoConfig) {
    if (defaultProcessor) {
      this.defaultProcessor = defaultProcessor;
    }
    if (errorHandler) {
      this.errorHandler = errorHandler;
    }
  }
}

const reduxRepo = new ReduxRepo();
export default reduxRepo;
